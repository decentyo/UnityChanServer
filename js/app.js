/// <reference path="../dt/node/node.d.ts"/>
/// <reference path="../dt/socket.io/socket.io.d.ts"/>
var socketIO = require('socket.io');
var io = socketIO(3000);
var clientsCount = 0;

io.on('connection', function (socket) {
    console.log("new connection");
    clientsCount++;
    socket.join('room');
    socket.emit('connected', {id: socket.id});

    var clients = io.sockets.adapter.rooms['room'];

    // if first client
    if (clientsCount === 1) {
        socket.emit('newServer', {id: socket.id});
    }

    for (var clientId in clients) {
        if (clients.hasOwnProperty(clientId)) {
            //var clientSocket = io.sockets.connected[clientId];
            if (socket.id !== clientId) {
                socket.emit('newClient', {id: clientId});
            }
        }
    }

    socket.broadcast.to('room').emit('newClient', {id: socket.id});

    socket.on('updateObject', function (data) {
        socket.broadcast.to('room').emit('updateObject', data);
    });

    socket.on('disconnect', function () {
        clientsCount--;
        io.emit('userDisconnected', {id: socket.id});
    });
});
